const express = require("express");
const router = express.Router();
const productController = require("../controllers/product");
const auth = require("../auth");

// Route for creating a Product
router.post("/", auth.verify, (req, res) => {

	const userData = auth.decode(req.headers.authorization);

	// Condition for isAdmin 

	if(userData.isAdmin == true){
		console.log(req.body)
		productController.addProduct(req.body).then(resultFromController =>
			res.send(resultFromController))
	} else{
		res.send(false);
	}
});

// Route for getting all Products
router.get("/all", (req, res) => {
	productController.getAllProducts().then(resultFromController => res.send(resultFromController))
});

// Route for getting all active Products
router.get("/allActive", (req, res) => {
	productController.getAllActiveProducts().then(resultFromController => res.send(resultFromController))
});

// Route for retrieving a specific Product
router.get("/:productId", (req, res) => {
	console.log(req.params.productId);

	productController.getProduct(req.params).then(resultFromController => res.send(resultFromController));
});

// Route for updating Product information
router.put("/:productId/", auth.verify, (req, res) => {
	
	// Added auth.decode and auth.verify for bearer token verification
	const userData = auth.decode(req.headers.authorization);
	if(userData.isAdmin == true){
		console.log(req.body)
		productController.updateProduct(req.params, req.body).then(resultFromController => res.send(resultFromController))
	} else {
		res.send(false);
	}
});

// Route for archiving a specific Product
router.put("/:productId/archive", auth.verify, (req, res) => {

	// Added auth.decode and auth.verify for bearer token verification
	const userData = auth.decode(req.headers.authorization);
	if(userData.isAdmin == true){
		console.log(req.body)
	productController.archiveProduct(req.params).then(resultFromController => res.send(resultFromController));
	} else {
		res.send(false);
	}
});

// Route for activating a specific Product
router.put("/:productId/activate", auth.verify, (req, res) => {
  const userData = auth.decode(req.headers.authorization);
  if (userData.isAdmin == true) {
    productController.activateProduct(req.params).then(resultFromController => res.send(resultFromController));
  } else {
    res.send(false);
  }
});


// Allows us to export the "router" object that will be acessed in our "index.js" file
module.exports = router;
