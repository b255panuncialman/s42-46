const express = require("express");
const router = express.Router();
const orderController = require("../controllers/order");
const auth = require('../auth.js');

// Check out
router.post("/checkout", auth.verify, (req, res) => {
  if (auth.decode(req.headers.authorization).isAdmin === true) {
    res.send(false);
  } else {
    const userId = auth.decode(req.headers.authorization).id;
    orderController.checkout(userId, req.body).then(resultFromController => res.send(resultFromController));
  }
});

// Retrieve authenticated user's orders
router.get("/myOrders", auth.verify, (req, res) => {
  const userData = auth.decode(req.headers.authorization);
  orderController.getOrders(userData.id).then(resultFromController => res.send(resultFromController));
});

// Retrieve all orders
router.get("/allOrders", auth.verify, (req, res) => {
  if (auth.decode(req.headers.authorization).isAdmin === false) {
    res.send(false);
  } else {
    orderController.getAllOrders().then(resultFromController => res.send(resultFromController));
  }
});

router.get("/subTotal", auth.verify, (req, res) => {
  const userData = auth.decode(req.headers.authorization);
  orderController.getSubtotal(userData.id).then(resultFromController => res.send(resultFromController));
});

router.get("/total", auth.verify, (req, res) => {
  const userData = auth.decode(req.headers.authorization);
  orderController.getTotal(userData.id).then(resultFromController => res.send(resultFromController));
});

// Allows us to export the "router" object that will be accessed in our "index.js" file
module.exports = router;
