const Product = require("../models/Product");


// Create a new Product

module.exports.addProduct = (reqBody) => {
  
  let newProduct = new Product({
    name : reqBody.name,
    description : reqBody.description,
    price : reqBody.price
  });

  // Saves the created Product object to our database
  return newProduct.save().then((product, error) => {
    // Product creation failed
    if(error) {
      return false;

    // Product creation successful
    } else {
      return true
    }
  })
};

// Retrieve all Products

module.exports.getAllProducts = () => {
  return Product.find().then(result => {
    return result;
  })
};

// Retrieve all active products
module.exports.getAllActiveProducts = () => {
  return Product.find({ isActive: true }).then(result => {
    return result;
  })
};

// Retrieve a specific Product

module.exports.getProduct = (reqParams) => {
  return Product.findById(reqParams.productId).then(result => {
    return result;
      })
};


// Update a specific Product
module.exports.updateProduct = (reqParams, reqBody) => {

  // Specify the fields/properties of the document to be updated
  let updateProduct = {
    name : reqBody.name,
    description : reqBody.description,
    price : reqBody.price
  }

  return Product.findByIdAndUpdate(reqParams.productId, updateProduct).then((product, error) =>{
    // Product is not updated
    if(error){
      return false;
    // Product updated successfully
    }else{
      return true;
    }
  })
};

// Archive a specific product
module.exports.archiveProduct = (reqParams) => {

  let updateActiveField = {
    isActive : false
  };

  return Product.findByIdAndUpdate(reqParams.productId, updateActiveField).then((product, error) => {

    // Product not archived
    if (error) {

      return false;

    // Product archived successfully
    } else {

      return true;

    }

  });
};

// Activate a specific product
module.exports.activateProduct = (reqParams) => {
  let updateActiveField = {
    isActive: true,
  };

  return Product.findByIdAndUpdate(reqParams.productId, updateActiveField).then((product, error) => {
    if (error) {
      return false;
    } else {
      return true;
    }
  });
};
